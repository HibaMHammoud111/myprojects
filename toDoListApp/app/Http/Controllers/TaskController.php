<?php

namespace App\Http\Controllers;

use App\Task;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userId= Auth::id();
        
        if($userId == 1) {
            $tasks = DB::table('tasks')
                ->join('users', 'users.id', '=', 'tasks.written_by')
                ->select('tasks.*', 'users.name')
                ->get();
            return view('home', [
                'tasks' => $tasks, 
                'admin'=> TRUE]);
        } else {
            $tasks = Task::orderBy('created_at', 'asc')->where('written_by', '=', ''.$userId.'')->get();
             return view('home', [
            'tasks' => $tasks,
            'admin' => FALSE]);     
        }
    }

    public function create()
    {
        return view('add');
    }
 
    public function store(Request $request)
    {
        $userId = Auth::id();
        $task = new Task();
        $task->title = $request::get('title');
        $task->description = $request::get('description');
        $task->written_by = $userId;
        $task->save();
        $task = Task::orderBy('created_at', 'desc')->first();
        return view('show')->with(['task' => $task]);   
    }

    public function show($id)
    {
        $tasks = Task::all();
        $task = $tasks->find($id);
        return view('show')->with(['task' => $task]);
    }

    public function edit($id)
    {
        $userId = Auth::id();
        $tasks = Task::all();
        $task = $tasks->find($id);
        $task_writer = $task->written_by;

        if($userId != $task_writer ) { 
            return view ('notPermitted');
        }
        return view('edit')->with(['task' => $task]);  
    }

  
    public function destroy($id)
    {
        $userId = Auth::id();
        $tasks = Task::all();
        $task = $tasks->find($id);
        $task_writer = $task->written_by;
     
        if($userId != $task_writer ) {   
            return view ('notPermitted');
        }
        $task->delete();
        return redirect('\home');
    }
}