<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Task;
use App\User;
Auth::routes();
Route::get('/', 'HomeController@index');
Route::get('/home', 'TaskController@index')->middleware('auth');
Route::get('/create', 'TaskController@create')->middleware('auth');
Route::post('/create','TaskController@store')->middleware('auth');
Route::get('/show/{id}', 'TaskController@show');
Route::get('/edit/{id}', 'TaskController@edit')->middleware('auth');
Route::get('/delete/{id}','TaskController@destroy')->middleware('auth');

Route::post('/edit/{id}', function ($id, Request $request) {
	$tasks = Task::all();
	$task = $tasks->find($id);
	$task->title = $request::get('title');
    $task->description = $request::get('description');
	$task->save();
	return redirect('\home');
});