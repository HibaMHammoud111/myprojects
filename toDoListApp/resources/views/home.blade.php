@extends('layouts.app') 

@section('content')

<div class="container">
    <div class="row">
         <div class="col-md-8 col-md-offset-2">
            <h1>To Do List</h1>
            @if (count($tasks) > 0)

            <div class="list-group">
                @foreach ($tasks as $task)

                <div class="list-group-item">
                    {{ $task->title }}
                    <a class="btn btn-danger btn-xs pull-right" href="{{url('/delete/'.$task->id)}}" type="button">delete</a> <a class="btn btn-success btn-xs pull-right" href="{{url('/edit/'.$task->id)}}" type="button">edit</a> <a class="btn btn-primary btn-xs pull-right" href="{{url('/show/'.$task->id)}}" type="button">view</a>
                    @if($admin)

                    <label>written by {{ $task->name }} </label>
                    @endif
                </div>
                @endforeach
            </div>
            @endif 
            <a class="btn btn-warning btn-block" href="{{url('/create')}}" type="button">Add Task</a>
        </div>
    </div>
</div>
@endsection
