@extends('layouts.app') 

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <form action="{{url('/edit/'.$task->id)}}" method="post">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="title">Title:</label> <input autofocus="" class="form-control" id="title" name="title" required="" type="text" value='{{ $task->title }}'><br>
                    <label for="description">Description:</label> 

                    <textarea class="form-control" id="description" name="description" rows="8">{{ $task->description }}</textarea><br>
                    <button class="btn btn-warning btn-block" type="submit">Save Task</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
