@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    <label for="title">Title:</label> <label for="title" id="title">{{ $task->title }}</label><br>
                    <label for="title">Description:</label> <label id="description">{{ $task->description }}</label><br>
                    <a class="btn btn-primary btn-block" href="{{url('/edit/'.$task->id)}}" type="button">Edit</a> <a class="btn btn-danger btn-block" href="{{url('/delete/'.$task->id)}}" type="button">Delete</a>

                    <div class="panel-footer pull-right">
                    created at:{{ $task->created_at}}, last updated at {{ $task->updated_at }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

