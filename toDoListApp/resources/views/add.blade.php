@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">  
            <form method="POST" action="{{ url('create') }}">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="title">Title:</label>
                    <input  name="title" type="text" class="form-control" id="title" required autofocus>
                    <br>
                    <label for="description">Description:</label>
                    <textarea name="description" class="form-control" rows="8" id="description"></textarea>
                    <br>
                    <button type="submit" class="btn btn-warning btn-block">Add Task</button>
                </div>
            </form>    
        </div>
    </div>
</div>
@endsection